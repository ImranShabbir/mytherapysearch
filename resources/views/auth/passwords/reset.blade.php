@extends('layouts.app')

@section('content')

<!-- <section class="visual-banner overflow-hidden registration-block" style="background-image: url(images/registration-bg.png);">
    <div class="upper-registration">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading text-center mb-5">
              <h1>Welcome to MyTherapySearch</h1>
              <p>Forgot Passward</p>
            </div>
          </div>
        </div>
        <div class="row animate" data-anim-type="fadeInUp">
          <div class="col-12 pb-5">
              <form>
                <div class="input-group mb-4">
                  <input type="email" class="form-control mr-md-2 mb-2 mb-md-0" id="email" placeholder="Email Address">
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-envelope"></span></div>
                  </div>
                </div>
                <div class="form-group pb-4">
                  <button type="submit" class="btn w-100 mb-3">Reset Passward</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <main>

  </main> -->

<div class="login-block overflow-hidden" style="background-image: url({{ URL::asset('images/login-bg.png')}})">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Resets Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
