<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
// if(Auth::check('user')){
//         print_r(Auth::guard('user'));
       
       

//        }
//        exit();
       
       //exit();
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {

        return view('auth.admin.login');
    }
    

    protected function attemptLogin(Request $request)
    {
        $is_admin_login = 'true';
       // Session::set('variableName', $value);
        return $this->guard('admin')->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function logout(Request $request) {
        Auth::guard('admin')->logout();
        //Session::flush();
        $request->session()->flush();
        //$request->session()->regenerate();
         // $request->session()->invalidate();
        return redirect('admin/login');
    }

    protected function authenticated(Request $request, $user)
    {
       $this->setUserSession($user);
    }

    protected function setUserSession($user)
        {
            session(
                [
                    'user_id' => $user->id,
                    'firstname' => $user->firstname,
                    'lastname' => $user->lastname,
                    'user_table'=>'admins'
                ]
            );
        }

    
}

