$(document).ready(function(e) {
$('.navbar-toggler').on('click', function(e) {
$('body').toggleClass('nav-open');
e.stopPropagation();
});
});
// below function is to remove class from body clicking anywhere (it is optional)
$(document).click(function(e) {
$('body').removeClass('nav-open');
});
jQuery(document).ready(function(e) {
jQuery('.navbar .nav-link').on('click', function(e) {
    jQuery('body').removeClass('nav-open');
});
});

// add fixed class in header

$(window).scroll(function(){
    var sticky = $('.sticky'),
      scroll = $(window).scrollTop();

    if (scroll >= 10) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
  });





// therapy Slider
$('.therapy-slider').slick({
speed: 5000,
autoplay: true,
autoplaySpeed: 0,
cssEase: 'linear',
slidesToShow: 3,
slidesToScroll: 1,
infinite: true,
arrows: false,
buttons: false,
responsive: [
{
breakpoint: 991.98,
settings: {
slidesToShow: 3
}
},
{
breakpoint: 767.98,
settings: {
slidesToShow: 2
}
},
{
breakpoint: 639,
settings: {
slidesToShow: 1
}
},
{
breakpoint: 479,
settings: {
slidesToShow: 1
}
}
]
});

// Filters
$(document).ready(function(){
    $('.filters label, .submenu a').click( 
        function () {
        $(this).parent().toggleClass('show');
        $(this).parent().siblings().removeClass('show');
    });
    $('.filters label').click(function(e) {
        e.stopPropagation();
    });

});
$(document).ready(function(){
    $('.filters-submenu a').click( 
        function () {
        $(this).parent().toggleClass('show');
    });
    $('.filters-submenu a').click(function(e) {
        e.stopPropagation();
    });

});


// Script for Smooth Scrolling
(function($) {      "use strict";
$(function() {
$('a[href*="#"]:not([href="#"])').click(function() {
//alert(this.pathname);
if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    $(".icon").removeClass('active');
    var target = $(this.hash);
    var hashVal = this.hash;
    target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    if (target.length) {
        console.log(hashVal);
        setTimeout(function(){console.log(hashVal);$(hashVal).find('.icon').trigger('click')},1000);
            jQuery('html, body').animate({
                scrollTop: target.offset().top - 70
            }, 1000);
            return false;
    }

}
});
});
}(jQuery));

