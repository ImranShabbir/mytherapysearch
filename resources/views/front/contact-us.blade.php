@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden visual-inner-banner" style="background-image: url(images/visual-inner-banner1.jpg);">
    <div class="container">
      <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
        <div class="col-12">
          <h1 class="text-center text-uppercase">Want More Information?</h1>
          <p>Email Us You Questions</p>
        </div>
      </div>
    </div>
  </section>
  <main>
    <section class="contact-us-block">
      <div class="container">
        <div class="row">
          <div class="col-md-6 animate" data-anim-type="fadeInLeft">
            <div class="heading">
              <span class="uper-head d-block position-relative text-uppercase">keep in touch</span>
              <h1>Let’s get started</h1>
            </div>
          </div>
          <div class="col-md-6 animate" data-anim-type="fadeInRight">
            <div class="form-hol">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputname1" class="text-uppercase">First Name</label>
                    <input type="text" class="form-control" id="inputname1">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="lastname" class="text-uppercase">Last Name</label>
                    <input type="text" class="form-control" id="lastname">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="text-uppercase">Email</label>
                    <input type="email" class="form-control" id="inputEmail">
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="phone" class="text-uppercase">Phone</label>
                  <input type="tel" id="phone" name="phone" class="form-control">
                  </div>
                  <div class="form-group col-md-6">
                  </div>
                </div>
                <div class="form-group">
                  <label for="subject" class="text-uppercase">Subject</label>
                  <input type="text" class="form-control" id="subject">
                </div>
                <div class="form-group last-field">
                  <label for="additionalinfo" class="text-uppercase">Additional info</label>
                  <textarea class="form-control" id="additional-info" rows="4"></textarea>
                </div>

                <button type="submit" class="btn text-uppercase">Submit</button>
              </form>
            </div>  
          </div>
        </div>
      </div>
    </section>

  </main>


@endsection