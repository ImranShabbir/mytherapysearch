
<footer id="footer" class="overflow-hidden">
    <div class="footer-block">
      <div class="container">
        <div class="row align-items-center animate" data-anim-type="fadeInUp">
          <div class="col-12">
            <div class="heading text-center">
              <h1>Want More Information?</h1>
            </div>
            <div class="subscription-blocK">
              <form class="subscribe" novalidate="novalidate" action="http://cratejoytemplates.com/staging/gd/cac/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                <div class="form-group">
                  <input class="form-control mage-error" name="email" type="email" id="newsletter" placeholder="Enter your email address">
                </div>
                  <div class="actions">
                    <input class="btn-action" type="submit" title="Subscribe" value="Subscribe">
                  </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row animate" data-anim-type="fadeInUp">
          <div class="col-lg-8">
            <nav class="footer-navbar">
              <ul class="d-md-flex align-items-center justify-content-start text-center pt-5 pb-4">
                <li class="nav-item">
                  <a class="nav-link" href="#lab">Match</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#services">Therapists</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#team">Articles</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#contact">Event</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#contact">Privacy Policy </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#contact">Terms of Service</a>
                </li>
              </ul>
            </nav>
          </div>
          <div class="col-lg-4 pt-lg-5 pb-4">
            <div class="mail-hol text-white">
              <span class="icon-envelope mr-3"></span><a href="mailto:Info@mytherapysearch.com" class="text-white" >Info@mytherapysearch.com</a>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="lower-footer pt-4 pb-4 animate" data-anim-type="fadeInUp">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-8">
            <div class="lower-wrap d-flex align-items-center">
              <div class="img-block mr-3"><img src="{{asset('images/lower-icon.png')}}" alt="icon-footer"></div>
              <div class="footer-content">
                <p class="mb-2">Copyright © 2020 <a href="#" class="text-white">MyTherapySearch</a>  and its affiliates.</p>
                <p class="mb-0">All rights reserved. Additional crisis resources can be found <a href="#" class="text-white">here</a> .</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <ul class="social d-flex align-items-center justify-content-center justify-content-md-end">
              <li><a href="#" class="icon-google-plus"></a></li>
              <li><a class="icon-facebook" href="#"></a></li>
              <li><a class="icon-linkedin" href="#"></a></li>
              <li><a class="icon-twitter" href="#"></a></li>
              <li><a class="icon-instagram" href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</footer>

