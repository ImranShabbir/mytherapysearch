@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden registration-block" style="background-image: url(images/registration-bg.png);">
    <div class="upper-registration">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="heading text-center mb-5">
              <h1>Welcome to MyTherapySearch</h1>
              <p>Sign Up To Continue</p>
            </div>
          </div>
        </div>
        <div class="row animate" data-anim-type="fadeInUp">
          <div class="col-12">
              <form>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mb-2 mb-md-0" id="first-name" placeholder="First Name">
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mb-2 mb-md-0" id="last-name" placeholder="Last Name">
                </div>
                <div class="input-group mb-4">
                  <select class="custom-select custom-select-lg form-control">
            <option selected>Insurance Accepted</option>
            <option value="1">Yes</option>
            <option value="2">No</option>
          </select>
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mb-2 mb-md-0" id="fees" placeholder="Fees">
                </div>
                <div class="input-group mb-4">
                  <select class="custom-select custom-select-lg form-control">
            <option selected>Education</option>
            <option value="1">Ph.D</option>
            <option value="2">P.SyD</option>
            <option value="3">LCSW</option>
            <option value="4">LMFT</option>
            <option value="5">LMHC</option>
          </select>
                </div>
                <div class="input-group mb-3">
                  <input type="text" class="form-control mb-2 mb-md-0" id="License" placeholder="License #">
                </div>
                <div class="input-group mb-4">
                  <textarea class="w-100 form-control" placeholder="Description"></textarea>
                </div>
                <div class="form-group pb-4">
                  <button type="submit" class="btn w-100 mb-3">Sign Up</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="lower-registration border-top pt-4 pb-5 animate" data-anim-type="fadeInUp">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="not-account text-center">
                <p>Already have an account? <a href="#">Log in</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <main>

  </main>

<main>
   <!-- Login Form -->
   <section class="login-block signp-block overflow-hidden" style="background-image: url(images/login-bg.png);">
     <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading text-center mb-lg-5">
            <h1 class="login-title text-center" id="staticBackdropLabel">Welcome To <span class="orange"> Mytherapy Search</span></h1>
            <p class="grey">Sign Up To Continue</p>
          </div>
        </div>
      </div>
       <div class="row">
         <div class="col-md-6 offset-md-3">
           <div class="signp-content">
              <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('firstname') is-invalid @enderror" id="firstname" placeholder="{{ __('First Name') }}" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                    @error('firstname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-admin"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('lastname') is-invalid @enderror" id="lastname" placeholder="{{ __('Last Name') }}" name="lastname" value="{{old('lastname')}}" required autocomplete="lastname" autofocus>
                    @error('lastname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-admin"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="text" class="form-control mr-md-2 mb-2 mb-md-0 @error('phone') is-invalid @enderror" id="phone" placeholder="{{ __('Phone') }}" name="phone" value="{{ old('phone') }}" required autocomplete="phone">
                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-phone"></span></div>
                  </div>
                </div>
                <div class="input-group mb-4">
                  <input type="email" class="form-control mr-md-2 mb-2 mb-md-0 @error('email') is-invalid @enderror" id="email" placeholder="{{ __('E-Mail Address') }}" name="email" value="{{ old('email') }}" required autocomplete="email">
                  @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-mail"></span></div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="password" class="form-control mr-md-2 mb-2 mb-md-0 @error('password') is-invalid @enderror" id="password" placeholder="{{ __('Create Password') }}" name="password" required autocomplete="new-password">
                  @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="Password" id="password-confirm" class="form-control mr-md-2 mb-2 mb-md-0" placeholder="{{ __('Confirm Password') }}" name="password_confirmation" required autocomplete="new-password">
                  
                  <div class="input-group-prepend">
                    <div class="input-group-text border-0"><span class="icon-padlock"></span></div>
                  </div>
                </div>
                <input type="hidden" name="user_type" value="therapist">
                <!-- <div class="form-group mb-lg-5 mb-3">
                  <label class="smart-label">I don't want to receive marketing messages from Go Service. I can also opt out of receiving these at any time in my account settings or via the link in the message.
                    <input type="checkbox">
                    <span class="checkmark"></span>
                  </label>
                </div> -->
                <div class="form-group border-bottom pb-4">
                  <button type="submit" class="btn w-100 mb-3">{{ __('Register') }} <span></span><span></span><span></span><span></span></button>
                  <!-- <a href="#" class="forgot-password text-center d-block dark-grey">Forgot password?</a> -->
                </div>
                <div class="form-group text-center ">
                 <p>Already have an account<span class="question">?</span> <a href="{{url('/login')}}" class="blue signp-btn">Log In</a></p> 
                </div>
              </form>
            </div>
         </div>
       </div>
     </div>
   </section>
  </main>




@endsection
