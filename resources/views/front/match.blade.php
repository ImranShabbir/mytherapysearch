@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden visual-inner-banner" style="background-image: url(images/visual-inner-banner4.jpg);">
    <div class="container">
      <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
        <div class="col-12">
          <h1 class="text-center text-uppercase">let's match you with a therapist</h1>
          <p>tell a little bit about what you're looking for</p>
        </div>
      </div>
    </div>
  </section>
  <main>

    <!-- Match Survey Block -->
    <section class="match-survey-block overflow-hidden custom-blog">
      <div class="container">
        <div class="row">
          <div class="col-12">Survey will go here</div>
        </div>
      </div>
    </section>

  </main>

@endsection