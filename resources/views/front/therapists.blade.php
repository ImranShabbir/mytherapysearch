@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden visual-inner-banner" style="background-image: url(images/visual-inner-banner2.jpg);">
    <div class="container">
      <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
        <div class="col-12">
          <h1 class="text-center text-uppercase">Meet The Top Therapists In NYC</h1>
          <p>All Of Our Therapists Offer A Fresh Perspective</p>
        </div>
      </div>
    </div>
</section>
<main>
    <!-- Filters -->
    <div class="filter-block">
      <div class="container">
        <h2>Select all that Apply</h2>
        <form action="" method="">
          <div class="form-row filters">
            <div class="form-group col-md-3 mb-4">
              <label class="head">Degree</label>
              <div class="dropdown">
                <ul>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Ph.D">
                      <label class="form-check-label" for="Ph.D">Ph.D</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="P.SyD">
                      <label class="form-check-label" for="P.SyD">P.SyD</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="LCSW">
                      <label class="form-check-label" for="LCSW">LCSW</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="LMFT">
                      <label class="form-check-label" for="LMFT">LMFT</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="LMHC">
                      <label class="form-check-label" for="LMHC">LMHC</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <label class="head">Treatment For:</label>
              <div class="dropdown">
                <ul>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Addiction</a>
                    <ul class="submenu-dropdown">
                      <li>See Specialties</li>
                    </ul>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Anxiety</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="OCD">
                          <label class="form-check-label" for="OCD">OCD</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="hoarding">
                          <label class="form-check-label" for="hoarding">Hoarding</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="panic-attacks">
                          <label class="form-check-label" for="panic-attacks">Panic Attacks</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="general-anxiety">
                          <label class="form-check-label" for="general-anxiety">General Anxiety</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="phobias">
                          <label class="form-check-label" for="phobias">Phobias</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="social-anxiety">
                          <label class="form-check-label" for="social-anxiety">Social Anxiety</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="excoriation">
                          <label class="form-check-label" for="excoriation">Excoriation (Skin-Picking)</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="trichotillomania">
                          <label class="form-check-label" for="trichotillomania">Trichotillomania (Hair-Pulling)</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="other">
                          <label class="form-check-label" for="other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="depression">
                      <label class="form-check-label" for="depression">Depression</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="relationship-issues">
                      <label class="form-check-label" for="relationship-issues">Relationship issues</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="stress">
                      <label class="form-check-label" for="stress">Stress</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="grief-loss">
                      <label class="form-check-label" for="grief-loss">Grief / Loss</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="career-counseling">
                      <label class="form-check-label" for="career-counseling">Career Counseling</label>
                    </div>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Family Issues</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="marital-premarital">
                          <label class="form-check-label" for="marital-premarital">Marital and Premarital</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="parenting">
                          <label class="form-check-label" for="parenting">Parenting</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="pregnancy-prenatal-postpartum">
                          <label class="form-check-label" for="pregnancy-prenatal-postpartum">Pregnancy, Prenatal, Postpartum</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="infertility">
                          <label class="form-check-label" for="infertility">Infertility</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="family-planning">
                          <label class="form-check-label" for="family-planning">Family planning</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="couples">
                          <label class="form-check-label" for="couples">Couples</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="child-adolescent">
                          <label class="form-check-label" for="child-adolescent">A Child or Adolescent</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="other">
                          <label class="form-check-label" for="other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="life-transitions">
                      <label class="form-check-label" for="life-transitions">Life Transitions</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="self-esteem">
                      <label class="form-check-label" for="self-esteem">Self Esteem</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="anger">
                      <label class="form-check-label" for="anger">Anger</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="suicidal-thoughts">
                      <label class="form-check-label" for="suicidal-thoughts">Suicidal Thoughts</label>
                    </div>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Health Issues</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="obesity">
                          <label class="form-check-label" for="obesity">Obesity</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="chronic-illness-pain">
                          <label class="form-check-label" for="chronic-illness-pain">Chronic Illness/Pain</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="self-harming">
                          <label class="form-check-label" for="self-harming">Self-Harming</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="weight-loss">
                          <label class="form-check-label" for="weight-loss">Weight Loss</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="sleep-insomnia">
                          <label class="form-check-label" for="sleep-insomnia">Sleep or Insomnia</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="sexual-dysfunction">
                          <label class="form-check-label" for="sexual-dysfunction">Sexual Dysfunction</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="other">
                          <label class="form-check-label" for="other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="bipolar">
                      <label class="form-check-label" for="bipolar">Bipolar</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="mood-swings">
                      <label class="form-check-label" for="mood-swings">Mood Swings</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="loss-interest-pleasure">
                      <label class="form-check-label" for="loss-interest-pleasure">Loss of Interest or Pleasure</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="self-harming">
                      <label class="form-check-label" for="self-harming">Self Harming</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <label class="head">Specialties</label>
              <div class="dropdown">
                <ul>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Addiction</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="alcohol-use">
                          <label class="form-check-label" for="alcohol-use">Alcohol Use</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="drug-use">
                          <label class="form-check-label" for="drug-use">Drug Use</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Pornography">
                          <label class="form-check-label" for="Pornography">Pornography</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="internet-use">
                          <label class="form-check-label" for="internet-use">Internet Use</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Gambling">
                          <label class="form-check-label" for="Gambling">Gambling</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Sexual">
                          <label class="form-check-label" for="Sexual">Sexual</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="video-games">
                          <label class="form-check-label" for="video-games">Video Games</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Other">
                          <label class="form-check-label" for="Other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="racial-identify-issues">
                      <label class="form-check-label" for="racial-identify-issues">Racial Identify Issues</label>
                    </div>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Trauma</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="sexual-abuse">
                          <label class="form-check-label" for="sexual-abuse">Sexual Abuse</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="PTSD">
                          <label class="form-check-label" for="PTSD">PTSD</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="domestic-violence-abuse">
                          <label class="form-check-label" for="domestic-violence-abuse">Domestic Violence / Abuse</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Other">
                          <label class="form-check-label" for="Other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Eating Disorders</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Bulimia">
                          <label class="form-check-label" for="Bulimia">Bulimia</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Anorexia">
                          <label class="form-check-label" for="Anorexia">Anorexia</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="binge-eating">
                          <label class="form-check-label" for="binge-eating">Binge Eating</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="restrictive-eating">
                          <label class="form-check-label" for="restrictive-eating">Restrictive Eating</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="excessive-exercise">
                          <label class="form-check-label" for="excessive-exercise">Excessive Exercise</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Other">
                          <label class="form-check-label" for="Other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="domerstic-abuse">
                      <label class="form-check-label" for="domerstic-abuse">Domerstic Abuse</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="personality-disorder">
                      <label class="form-check-label" for="personality-disorder">Personality Disorder</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="gender-identifty-issues">
                      <label class="form-check-label" for="gender-identifty-issues">Gender Identifty Issues</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="ADHD">
                      <label class="form-check-label" for="ADHD">ADHD</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Couples">
                      <label class="form-check-label" for="Couples">Couples</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="OCD">
                      <label class="form-check-label" for="OCD">OCD</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="children-teens-tweens">
                      <label class="form-check-label" for="children-teens-tweens">Children / Teens / Tweens</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <label class="head">Locations</label>
              <div class="dropdown">
                <ul>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Manhattan</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="midtown-manhattan">
                          <label class="form-check-label" for="midtown-manhattan">Midtown Manhattan</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="lower-manhattan">
                          <label class="form-check-label" for="lower-manhattan">Lower Manhattan</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="villages-soho">
                          <label class="form-check-label" for="villages-soho">The Villages and Soho</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="union-square-madison-park">
                          <label class="form-check-label" for="union-square-madison-park">Union Square / Madison Park</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="upper-east-side">
                          <label class="form-check-label" for="upper-east-side">Upper East Side</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="upper-west-side">
                          <label class="form-check-label" for="upper-west-side">Upper West Side</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Brooklyn</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="williamsburg-greenpoint">
                          <label class="form-check-label" for="williamsburg-greenpoint">Williamsburg / Greenpoint</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="park-slope">
                          <label class="form-check-label" for="park-slope">Park Slope</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="brooklyn-heights">
                          <label class="form-check-label" for="brooklyn-heights">Brooklyn Heights</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Queens</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="long-island-city">
                          <label class="form-check-label" for="long-island-city">Long Island City</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Astoria">
                          <label class="form-check-label" for="Astoria">Astoria</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Telehealth">
                      <label class="form-check-label" for="Telehealth">Telehealth</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <label class="head">Insurance / Fees</label>
              <div class="dropdown">
                <ul>
                    <li class="filters-submenu">
                    <a href="JavaScript:;">In Network</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Aetna">
                          <label class="form-check-label" for="Aetna">Aetna</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="blue-cross-blue-shield">
                          <label class="form-check-label" for="blue-cross-blue-shield">Blue Cross / Blue Shield</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Cigna">
                          <label class="form-check-label" for="Cigna">Cigna</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="UnitedHealthCare">
                          <label class="form-check-label" for="UnitedHealthCare">UnitedHealthCare</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="Other">
                          <label class="form-check-label" for="Other">Other</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="out-network">
                      <label class="form-check-label" for="out-network">Out of Network</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="no-insurance">
                      <label class="form-check-label" for="no-insurance">No Insurance</label>
                    </div>
                  </li>
                  <li class="filters-submenu">
                    <a href="JavaScript:;">Out of Pocket</a>
                    <ul class="submenu-dropdown">
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="price1">
                          <label class="form-check-label" for="price1">$0 - $50</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="price2">
                          <label class="form-check-label" for="price2">$50 - $100</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="price3">
                          <label class="form-check-label" for="price3">$100 - $200</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="price4">
                          <label class="form-check-label" for="price4">$200 - $300</label>
                        </div>
                      </li>
                      <li>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="price5">
                          <label class="form-check-label" for="price5">$300 and greater</label>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <label class="head">Identifies as</label>
              <div class="dropdown">
                <ul>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="black-african-american">
                      <label class="form-check-label" for="black-african-american">Black or African American</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="White">
                      <label class="form-check-label" for="White">White</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Asian">
                      <label class="form-check-label" for="Asian">Asian</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="hispanic-latino">
                      <label class="form-check-label" for="hispanic-latino">Hispanic / Latino</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Gay">
                      <label class="form-check-label" for="Gay">Gay</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="LGBTQ+">
                      <label class="form-check-label" for="LGBTQ+">LGBTQ+</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Lesbian">
                      <label class="form-check-label" for="Lesbian">Lesbian</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Bisexual">
                      <label class="form-check-label" for="Bisexual">Bisexual</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Male">
                      <label class="form-check-label" for="Male">Male</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="Female">
                      <label class="form-check-label" for="Female">Female</label>
                    </div>
                  </li>
                  <li>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" id="non-bianary-gender">
                      <label class="form-check-label" for="non-bianary-gender">Non-Bianary Gender</label>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div class="form-group col-md-3 mb-4">
              <button class="btn" type="submit">Search</button>
            </div>
          </div>
        </form>
        </div>
      </div>

    <!-- therapist cards -->
    <div class="therapist-block">
      <div class="container">
        <div class="row">
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3 mb-4">
            <div class="card">
              <div class="img-hol">
                <img src="{{asset('images/therapist1.png')}}" class="card-img-top" alt="latest-images">
              </div>
              <div class="button-hol">
                <a href="#" class="btn bg-white">View Profile</a>
              </div>
              <div class="card-body text-center">
                <h5 class="card-title">LINA PERL, PH.D.</h5>
                <p>Often, we seek therapy when <br>we feel overwhelmed</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Pagination -->
    <div class="pagination-block">
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          <li class="prev">
            <a class="page-link" href="#" aria-label="Previous">
              <span aria-hidden="true"><span class="icon-left"></span> PREV</span>
            </a>
          </li>
          <li class="page-item active"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">4</a></li>
          <li class="page-item"><a class="page-link" href="#">5</a></li>
          <li class=" next">
            <a class="page-link" href="#" aria-label="Next">
              <span aria-hidden="true">NEXT <span class="icon-right"></span></span>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </main>

@endsection