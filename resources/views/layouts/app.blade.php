<!doctype html>
<html lang="en">
	<head>
		<!-- Meta Tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet"> 
		<link href="{{URL::asset('images/favicon.png')}}" rel="icon" />
		<!-- Slick Slider CSS -->
		<link rel="stylesheet" href="{{ URL::asset('css/slick.css') }}">
		<!-- Boostrap -->
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
		<!-- Main CSS -->
		<link rel="stylesheet" href="{{ URL::asset('css/main.css') }}"> 
		<!-- Animation CSS -->
		<link rel="stylesheet" href="{{URL::asset('css/animation.min.css')}}">

		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		@yield('css')

	</head>
	<body class="body">
		<div id="wrapper">
			@include('front.header') 
			
			  	@yield('content')
			
		 	@include('front.footer')
	 	</div>
	 	<script src="{{ URL::asset('js/jquery-3.1.0.js') }}"></script>
	 	<script src="{{ URL::asset('js/animations.min.js') }}"></script>
		<script src="{{ URL::asset('js/slick.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('js/popper.min.js') }}"></script>
		<script src="{{ URL::asset('js/custom.js') }}"></script>
	</body>
</html>