@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden visual-inner-banner" style="background-image: url(images/visual-inner-banner4.jpg);">
    <div class="container">
      <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
        <div class="col-12">
          <h1 class="text-center text-uppercase">MyTherapySearch Connects <br> Patients And Therapists</h1>
          <div class="btn-hol text-center">
            <a href="{{url('login')}}" class="btn text-uppercase">Join Now</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <main>

    <!-- mental-proffessioal -->
    <div class="mental-proffessionals text-center">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <strong class="d-block">CREATED BY MENTAL HEALTH PROFESSIONALS, FOR MENTAL HEATH PROFESSIONALS TO INCREASE YOUR ONLINE PRESENCE, AND HELP YOU GROW YOUR BUSINESS SO YOU CAN FOCUS ON YOUR PATIENTS.</strong>
          </div>
        </div>
      </div>
    </div>

    <!-- Easy therapy -->
    <section class="custom-therapy seagreen-bg overflow-hidden">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6">
            <div class="easy-wrap animate" data-anim-type="fadeInLeft" data-anim-delay="1000">
              <h1 class="text-uppercase">About Us</h1>
              <p class="mb-lg-0 mb-md-3 pr-lg-3">MyTherapySearch is a referral service to connect you the therapist with patients that are right for you. As therapists ourselves, we understand the challenges involved in getting new patients.  We created this site as an answer to what we see as a industry wide problem: we don’t get the patients that we need, and we spend a lot of time speaking with patients that may not be right for our practice.  </p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="img-hol animate" data-anim-type="fadeInRight" data-anim-delay="1000">
              <img src="{{asset('images/easy1.png')}}" alt="image-description">
            </div>
          </div>
        </div>
      </div>
    </section>

     <!-- Take a look -->
    <section class="custom-therapy yellow-bg overflow-hidden">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-4 mb-md-0">
            <div class="img-hol animate" data-anim-type="fadeInLeft">
              <img src="{{asset('images/join1.png')}}" alt="image-description">
            </div>
          </div>
          <div class="col-md-6">
            <div class="easy-wrap animate" data-anim-type="fadeInRight">
              <h1 class="text-uppercase">Let Us Help Find The <br> Patients You Are <br> Looking For.  </h1>
              <p class="mb-lg-5 mb-md-3">With MyTherapySapce, we filter all patient requests and direct them to the therapists that match their search criteria. By joining MyTherapySearch you will have more time to focus on your patients and your practice. Our matching quiz effectively filters the patients you want for your practice. Patients contact you directly with an initiating email (MyTherapySearch is cc’d) allowing you to manage your contact and scheduling with the patient. We follow up with the patient to ensure they have been served in a timely and satisfying manner.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Three cards -->
    <div class="three-cards">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 animate mb-4 mb-md-0" data-anim-type="fadeInLeft">
            <div class="card-wrap">
              <div class="card bg-light-green">
                  <div class="icon-hol">
                    <span class="icon-link"></span>
                  </div>
                  <div class="card-body text-center">
                    <h5 class="card-title">Why Join?</h5>
                    <strong class="d-block mb-3">There is strength in numbers.</strong>
                    <p class="card-text">With numerous therapists, articles, events, and managed SEO, <br> MyTherapySpace creates a platform with its own gravity that patients can easily find and navigate.</p>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-lg-4 animate mb-4 mb-md-0" data-anim-type="fadeInUp">
            <div class="card-wrap">
              <div class="card pink-bg">
                  <div class="icon-hol">
                    <span class="icon-maintenance"></span>
                  </div>
                  <div class="card-body text-center">
                    <h5 class="card-title">How Does It Work?</h5>
                    <strong class="d-block  mb-3">Create your webpage.</strong>
                    <p class="card-text">By creating your own webpage on MyTherapySearch, you get sorted into the matching quiz and gallery filters. Patients find your profile page and contact you directly. </p>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-lg-4 animate mb-4 mb-md-0" data-anim-type="fadeInRight">
            <div class="card-wrap">
              <div class="card seagreen-bg">
                  <div class="icon-hol">
                    <span class="icon-scholarship"></span>
                  </div>
                  <div class="card-body text-center">
                    <h5 class="card-title">What Are The Fees?</h5>
                    <strong class="d-block mb-3">This is a subscription based service.</strong>
                    <p class="card-text">Members that sign up to create a webpage pay a monthly or annual fee. There are no additional fees. For patients looking for therapists the site is free.  </p>
                  </div>
                </div>
            </div>
          </div>
        </div>  
      </div>
    </div>


    <!-- two grids sections -->
    <section class="double-grids join-double-grid">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="easy-wrap animate" data-anim-type="fadeInLeft">
              <h1 class="text-uppercase">Member Benefits</h1>
              <p class="mb-lg-5 mb-md-3">By joining MyTherapySearch, you will have <br> access to the following:</p>
              <strong class="d-block pr-lg-5">
                A Unique Searchable Webpage <br> Manage Your  Webpage Content <br>Consulting And Support Services For Your Webpage <br> Select Keywords To Identify Your Patient Groups Direct Contact With Patients Post Your Events <br> Write An Article For The Blog And Get Noticed <br>Have Access To A Network Of Professionals
              </strong>
            </div>
          </div>
          <div class="col-md-6">
            <div class="easy-wrap2 animate" data-anim-type="fadeInRight">
              <h1  class="text-uppercase">Fees And Costs</h1>
              <p class="mb-lg-5 mb-md-3">MyTherapySearch a subscription based service. Fees are monthly or annual. Your patients are your patients. We do not take a finding fee, nor do we take booking fees.</p>
              <div class="pricing mb-5">
                <strong class="d-block">First 3 Months Free With Sign Up.</strong>
                <strong class="d-block">Monthly Fee: $30.00</strong>
                <strong class="d-block">Annual Fee: $300.00</strong>
              </div>
              <div class="btn-hol">
                <a href="{{url('login')}}" class="btn text-uppercase">Join NOW</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> 

  </main>


@endsection