@extends('layouts.app')

@section('content')

 <section class="visual-banner overflow-hidden visual-inner-banner" style="background-image: url(images/visual-inner-banner3.jpg);">
    <div class="container">
      <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
        <div class="col-12">
          <h1 class="text-center text-uppercase">ARTICLES FOR SELF <br> IMPROVEMENT</h1>
        </div>
      </div>
    </div>
  </section>
  <main>

    <!-- Latest Blog -->
    <section class="therapy-block overflow-hidden custom-blog">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest1.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest2.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest3.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest1.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest2.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
                <div class="card">
                  <div class="img-hol">
                    <img src="{{asset('images/latest3.png')}}" class="card-img-top" alt="latest-images">
                  </div>
                  <div class="card-body">
                    <h5 class="card-title">Understanding Insurance</h5>
                    <p class="mb-4 card-text">Simplifying the always confusing world of health insurance.</p>
                    <p class="card-text mb-4">May 14, 2020</p>
                    <div class="btn-hol text-center">
                      <a href="#" class="btn">View Detail</a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>

  </main>


@endsection