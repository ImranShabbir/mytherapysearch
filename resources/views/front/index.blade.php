@extends('layouts.app')

@section('content')

<section class="visual-banner overflow-hidden" style="background-image: url({{ URL::asset('images/visual-bg.jpg')}})">
  <div class="container">
    <div class="row align-items-start text-center animate" data-anim-type="fadeInUp">
      <div class="col-12">
        <h1 class="text-center text-uppercase">We Hear You’re  <br> Looking For A Therapist</h1>
        <h3>Let’s Connect You to The Best Therapists In NYC.</h3>
        <div class="btn-hol text-center">
          <a href="#learnmore" class="btn text-uppercase">Meet Our Therapists</a>
        </div>
      </div>
    </div>
  </div>
</section>
<main>

  <!-- Find right therapist section -->
  <section class="find-therapist overflow-hidden">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading text-center">
            <h1>LET US FIND THE RIGHT THERAPIST <br>FOR YOU. text</h1>
          </div>
        </div>
      </div>
      <div class="row animate" data-anim-type="fadeInUp">
        <div class="col-md-3 mb-4 mb-md-0">
          <div class="icon-hol icon-question text-white blue-bg mb-4"></div>
          <div class="content text-center">
            <p>TAKE OUR MATCHING <br>QUIZ</p>
          </div>
        </div>
        <div class="col-md-3 mt-md-5  mb-4 mb-md-0">
          <div class="icon-hol icon-search text-white red-bg mb-4"></div>
          <div class="content text-center">
            <p>REVIEW YOUR <br> SELECTIONS</p>
          </div>
        </div>
        <div class="col-md-3 mt-md-5  mb-4 mb-md-0">
          <div class="icon-hol icon-mail text-white bg-mustard mb-4"></div>
          <div class="content text-center">
            <p>EMAIL THE THERAPISTS <br> YOU LIKE</p>
          </div>
        </div>
        <div class="col-md-3  mb-4 mb-md-0">
          <div class="icon-hol icon-hours text-white green-bg mb-4"></div>
          <div class="content text-center">
            <p>GET A RESPONSE WITHIN <br>24 HOURS </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Easy therapy -->
  <section class="custom-therapy seagreen-bg overflow-hidden">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="easy-wrap animate" data-anim-type="fadeInLeft" data-anim-delay="1000">
            <h1>We Make Finding A <br> Therapist Easy</h1>
            <p class="mb-lg-5 mb-md-3">Created By Therapists To Connect You To The Right Professional Our Free 5 Minute Matching Quiz Filters The Right Therapists For You.</p>
            <div class="btn-hol">
              <a href="#" class="btn text-uppercase">TAKE THE MATCHING QUIZ NOW!</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img-hol animate" data-anim-type="fadeInRight" data-anim-delay="1000">
            <img src="{{asset('images/easy1.png')}}" alt="image-description">
          </div>
        </div>
      </div>
    </div>
  </section>

   <!-- Searching hard -->
  <section class="custom-therapy pink-bg overflow-hidden">
    <div class="container">
      <div class="row flex-row-reverse">
        <div class="col-md-6">
          <div class="easy-wrap animate" data-anim-type="fadeInRight">
            <h1>We Know Searching <br>Is Hard!</h1>
            <p class="mb-lg-5 mb-md-3">The Biggest Obstacle To Starting Therapy Is Finding The Right Therapist And Knowing The Right Questions To Ask.</p>
            <div class="btn-hol">
              <a href="#" class="btn text-uppercase">WANT MORE INFORMATION?</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img-hol animate" data-anim-type="fadeInLeft">
            <img src="{{asset('images/therapy1.png')}}" alt="image-description">
          </div>
        </div>
      </div>
    </div>
  </section>

   <!-- Take a look -->
  <section class="custom-therapy yellow-bg overflow-hidden">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="easy-wrap animate" data-anim-type="fadeInLeft">
            <h1>Take A Look </h1>
            <strong class="sub-title d-block mb-4">Our Diverse Group Of Therapists Specialize In Your <br>Particular Needs</strong>
            <p class="mb-lg-5 mb-md-3">Our members include psychotherapists, psychologists, social workers, counselors, and analysts Specialties include anxiety treatment, depression, couples therapy, stress, relationships and much more Located in new york city including manhattan and brooklyn.</p>
            <div class="btn-hol">
              <a href="#" class="btn text-uppercase">MEET OUR THERAPISTS!</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="img-hol animate" data-anim-type="fadeInRight">
            <img src="{{asset('images/therapy2.png')}}" alt="image-description">
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- two grids sections -->
  <div class="double-grids">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="easy-wrap animate" data-anim-type="fadeInLeft">
            <h2>ARE YOU A MENTAL HEALTH <br>PROFESSIONAL? </h2>
            <p class="mb-lg-5 mb-md-3">ARE YOU LOOKING TO INCREASE YOUR INTERNET PRESENCE? <br> WE CAN HELP!</p>
            <div class="btn-hol">
              <a href="#" class="btn text-uppercase">LEARN MORE AND JOIN TODAY!</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="easy-wrap2 animate" data-anim-type="fadeInRight">
            <h2>ARE YOU LOOKING FOR AN <br> OFFICE SPACE? </h2>
            <p class="mb-lg-5 mb-md-3">DO YOU NEED TO RENT OR SUBLET YOUR OFFICE SPACE? MyTherapyOfficeSpace.Com Has The Right Space For You.</p>
            <div class="btn-hol">
              <a href="#" class="btn text-uppercase">GO TO MyTherapyOfficeSpace NOW!</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  

  <!-- Latest Blog -->
  <section class="therapy-block overflow-hidden">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading text-center">
            <h1 class="text-uppercase">latest from the blog</h1>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="therapy-slider">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest1.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-hol animate" data-anim-type="fadeInUp">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest2.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-hol animate" data-anim-type="fadeInRight">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest3.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </section>

   <!-- upcomming Events -->
  <section class="therapy-block pt-0 overflow-hidden">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading text-center">
            <h1>UPCOMING EVENTS</h1>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="therapy-slider">
            <div class="content-hol animate" data-anim-type="fadeInLeft">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest4.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-hol animate" data-anim-type="fadeInUp">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest5.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-hol animate" data-anim-type="fadeInRight">
              <div class="card">
                <div class="img-hol">
                  <img src="{{asset('images/latest6.png')}}" class="card-img-top" alt="latest-images">
                </div>
                <div class="card-body">
                  <h5 class="card-title">MINDFULNESS GROUP With Dr. Rachel Diamond, Ph.D. Of Therapists Of New York</h5>
                  <p class="card-text">Friday, May 14, 2020   5:00 PM - 6:00 PM</p>
                  <div class="btn-hol text-center">
                    <a href="#" class="btn">View Detail</a>
                  </div>
                </div>
              </div>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </section>

</main>
@endsection
