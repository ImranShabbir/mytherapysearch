<!-- Header -->

<!-- {{url('/login')}} -->
<header id="header" class="position-fixed sticky w-100">
  <div class="container">
    <div class="row ">
      <div class="col-12">
        <div class="header-block d-flex justify-content-between align-items-center">
          <div class="logo">
            <a class="navbar-brand d-flex justify-content-center m-0 p-0" href="{{URL::to('/')}}">
                <img src="{{asset('images/logo.png')}}" alt="site logo">
            </a>
          </div>
          <nav class="navbar navbar-expand-lg">
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="bar top"></span>
                <span class="bar middle"></span>
                <span class="bar bottom"></span>
            </button>
            <div class="row">
              <div class="col-12">
               <div class="collapse navbar-collapse" id="navbar">
                  <ul class="navbar-nav d-lg-flex justify-content-end">
                    <li class="nav-item {{ (request()->is('match')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/match')}}">Match</a> </li>
                    <li class="nav-item {{ (request()->is('therapists')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/therapists')}}">Therapists</a> </li>
                    <li class="nav-item {{ (request()->is('articles')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/articles')}}">Articles</a> </li>
                    <li class="nav-item {{ (request()->is('events')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/events')}}">Events</a> </li>
                    <li class="nav-item {{ (request()->is('contact-us')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/contact-us')}}">Contact Us</a> </li>
                    <li class="nav-item {{ (request()->is('learn-more')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/learn-more')}}">Learn More</a> </li>
                    <li class="nav-item {{ (request()->is('join-now')) ? 'active' : '' }}"><a class="nav-link text-capitalize" href="{{url('/join-now')}}">Join Now</a> </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>




