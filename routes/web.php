<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	return view('front.index');

});

//Auth::routes();

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

//admin middleware routes

Route::get('/admin/login','Auth\Admin\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login','Auth\Admin\AdminLoginController@login');
Route::post('/admin/logout','Auth\Admin\AdminLoginController@logout')->name('admin.logout');
Route::post('/admin/password/email','Auth\Admin\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('/admin/password/reset', 'Auth\Admin\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('/admin/password/reset','Auth\Admin\AdminResetPasswordController@reset')->name('admin.password.update');
Route::get('/admin/password/reset/{token}', 'Auth\Admin\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
Route::get('/admin/register','Auth\Admin\AdminRegisterController@showRegistrationForm')->name('admin.register');
Route::post('/admin/register','Auth\Admin\AdminRegisterController@register');
Route::get('/admin/email/verify','Auth\Admin\AdminVerificationController@show')->name('admin.verification.notice');
Route::get('admin/email/verify/{id}','Auth\Admin\AdminVerificationController@verify')->name('admin.verification.verify');
Route::get('/admin/email/resend','Auth\admin\AdminVerificationController@resend')->name('admin.verification.resend');
Route::get('/admin/dashboard', 'AdminHomeController@index')->name('admin.dashboard')->middleware('admin.verified:admin,admin.verification.notice');



Route::group(['middleware' => ['auth:admin']], function () { 

	/*admin service Routes*/
	
	Route::get('/admin/services/all' , 'admin\services\ServiceController@index')->name('allServices');
	Route::post('/admin/service/create' , 'admin\services\ServiceController@create')->name('addServiceType');
	Route::get('/admin/service/type/add' , 'Admin\services\ServiceController@add')->name('addService');
	Route::get('/admin/service/edit/{id}' , 'Admin\services\ServiceController@edit')->name('editService');
	Route::post('/admin/service/update' , 'Admin\services\ServiceController@update')->name('updateService');
	Route::post('/admin/service/delete' , 'Admin\services\ServiceController@destroy')->name('deleteService'); 

	/*admin business flow and sub-categories route*/

	Route::get('/admin/businessflow/add' , 'admin\businessflow\BusinessflowController@index')->name('getServices');
	

});

Route::get('/match', function () {
	return view('front.match');
});

Route::get('/therapists', function () {
	return view('front.therapists');
});

Route::get('/articles', function () {
	return view('front.articles');
});

Route::get('/events', function () {
	return view('front.events');
});

Route::get('/contact-us', function () {
	return view('front.contact-us');
});

Route::get('/learn-more', function () {
	return view('front.learn-more');
});

Route::get('/join-now', function () {
	return view('front.join-now');
});



