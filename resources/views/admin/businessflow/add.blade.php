@extends('layouts.admindashboard')
@section('content')


  <!-- Hero -->
  <div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Business Flow</h1>
            <!-- <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">Tables</li>
                    <li class="breadcrumb-item active" aria-current="page">DataTables</li>
                </ol>
            </nav> -->
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <div class="container">
    
        <div class="col-md-6">
            <form action="{{ action('CourseController@course_outline_data')}}" method="POST" id="treeviewform">
                
                <div class="form-group">

                    <label for="parent_category_id">Select course</label>
                    <select class="form-control" id="course_type" name="course_type">
                        <option value="1">Beginner</option>
                        <option value="2">Intermediate</option>
                        <option value="3">Advance</option>
                    </select>
                </div>

                <div id="testShow"></div>

                <div class="form-group">

                    <label for="parent_outline_id">Select Parent Category</label>
                    <select class="form-control" id="parent_outline_id" name="parent_outline_id">
                        <option value="NULL">Parent Category</option>
                        
                    </select>
                </div> 
                <div class="form-group" id="datarequired">
                    <label for="outline_name">Enter category name</label>
                    <input type="text" class="form-control" id="outline_name" name="outline_name" required>
                    <span class="error">This field is required</span>
                </div>
                <div class="form-group">
                  
                  <p id="category-error"></p>
                </div>
                <input type="hidden" value="1" name="course_id">
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
                <meta name="_token" content="{{ csrf_token() }}">
                <input type="submit" value="Add" class="btn btn-info" id="ajaxSubmit">
            </form>
        </div>
        <div class="col-md-6">

          <ul id="completeCourseOutline"></ul>
        <!-- <h3 align="center"><u>Category Tree</u></h3> -->
        <br />
        <div id="treeview"></div>
            <div id="div1"></div>

        </div> 
    </div>
</div>
<!-- END Page Content -->


@endsection