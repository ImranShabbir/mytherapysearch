@extends('layouts.admindashboard')
@section('content')
{{-- <link rel="stylesheet" href="{{ URL::asset('assets/js/plugins/datatables/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ URL::asset('assets/js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.css') }}"> --}}

<!-- Hero -->
<div class="bg-body-light">
    <div class="content content-full">
        <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
            <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Services</h1>
        </div>
    </div>
</div>
<!-- END Hero -->

<!-- Page Content -->
<div class="content">
    <!-- Dynamic Table Full -->
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">All Services</h3>
        </div>

        @if ($message = Session::get('deleted'))

        <div class="alert alert-success alert-block">

            <button type="button" class="close" data-dismiss="alert">×</button>

            <strong>{{ $message }}</strong>

        </div>

        @endif


        <div class="block-content block-content-full">
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center" width="10%">#</th>
                        <th width="80%">Service Name</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count = 1; ?>
                    @foreach($services as $service)
                    <tr>
                        <td class="text-center" width="10%"><?php echo $count;?></td>
                        <td class="font-w600" width="80%">
                            {{$service->service_name}}
                        </td>

                        <td class="text-center" width="10%"> 
                            <div class="btn-group">
                                <a href="{{URL('admin/service/edit/'.$service->id)}}"><button type="button"
                                        class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title=""
                                        data-original-title="Edit">
                                        <i class="fa fa-pencil-alt"></i>
                                    </button></a>&nbsp;
                                <a href="javascript:" onclick="deleteService({{$service->id}})"><button type="button"
                                        class="btn btn-sm btn-primary js-tooltip-enabled" data-toggle="tooltip" title=""
                                        data-original-title="Delete">
                                        <i class="fa fa-times"></i>
                                    </button></a>
                            </div>
                        </td>
                    </tr>
                    <?php $count++; ?>
                    @endforeach

                </tbody>
            </table>

        </div>
    </div>
    <!-- END Dynamic Table Full -->

</div>
<!-- END Page Content -->


@endsection

